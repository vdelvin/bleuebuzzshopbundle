<?php

namespace Bleuebuzz\ShopBundle\Controller;

use Bleuebuzz\IntegrationBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

use Bleuebuzz\ShopBundle\Entity\CategoryInterface;
use Bleuebuzz\ShopBundle\Entity\ProductInterface;

use Bleuebuzz\ShopBundle\Entity\BaseCategory as Category;
use Bleuebuzz\ShopBundle\Entity\BaseProduct as Product;
use Bleuebuzz\ShopBundle\Form\BaseCategoryType;

class AdminBaseCategoryController extends BaseController
{
    /**
     * Lists all BaseCategory entities.
     */
    public function indexAction(Request $request)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('BleuebuzzShopBundle:BaseCategory');
        $entities = $repo->getRootNodes();
        $htmlTree = $repo->childrenHierarchy();

        return $this->render('BleuebuzzShopBundle:AdminBaseCategory:index.html.twig', array(
            'entities' => $entities,
            'htmlTree' => $htmlTree
        ));
    }

    /**
     * Finds and displays a BaseCategory entity.
     */
    public function showAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BleuebuzzShopBundle:BaseCategory')->findOneBySlug($slug);

        if (!$entity)
        {
            throw $this->createNotFoundException('Unable to find BaseCategory entity.');
        }

        return $this->render('BleuebuzzShopBundle:AdminBaseCategory:show.html.twig', array('entity' => $entity));
    }

    /**
     * Creates a new BaseCategory entity.
     */
    public function createAction(Request $request)
    {
        $entity = new Category();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_show', array('slug' => $entity->getSlug())));
        }

        return $this->render('BleuebuzzShopBundle:AdminBaseCategory:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a BaseCategory entity.
    *
    * @param BaseCategory $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(CategoryInterface $entity)
    {
        $form = $this->createForm(new BaseCategoryType(), $entity, array(
            'action' => $this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Category entity.
     *
     */
    public function newAction()
    {
        $entity = new Category();
        $form   = $this->createCreateForm($entity);

        return $this->render('BleuebuzzShopBundle:AdminBaseCategory:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing BaseCategory entity.
     *
     * @param string $slug
     */
    public function editAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BleuebuzzShopBundle:BaseCategory')->findOneBySlug($slug);

        if (!$entity)
        {
            throw $this->createNotFoundException('Unable to find BaseCategory entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('BleuebuzzShopBundle:AdminBaseCategory:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a BaseCategory entity.
    *
    * @param BaseCategory $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CategoryInterface $entity)
    {
        $form = $this->createForm(new BaseCategoryType(), $entity, array(
            'action' => $this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_update', array('slug' => $entity->getSlug())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing BaseCategory entity.
     */
    public function updateAction(Request $request, $slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BleuebuzzShopBundle:BaseCategory')->findOneBySlug($slug);

        if (!$entity)
        {
            throw $this->createNotFoundException('Unable to find BaseCategory entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {
            $em->flush();

            return $this->redirect($this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_edit', array('slug' => $entity->getSlug())));
        }

        return $this->render('BleuebuzzShopBundle:AdminBaseCategory:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
     * Deletes a BaseCategory entity.
     */
    public function deleteAction(Request $request, $slug)
    {
        $form = $this->createDeleteForm($slug);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $repo = $em->getRepository('BleuebuzzShopBundle:BaseCategory');
            $entity = $repo->findOneBySlug($slug);

            if (!$entity)
            {
                throw $this->createNotFoundException('Unable to find BaseCategory entity.');
            }

            $categoryManager = $this->container->get('bleuebuzz_shop.category_manager');

            $categoryManager->nodeRemove($entity);
        }

        return $this->redirect($this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_index'));
    }

    /**
     * Deletes a BaseCategory entity and her children
     */
    public function recursiveDeleteAction(Request $request, $slug)
    {
        $form = $this->createDeleteForm($slug, TRUE);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $repo = $em->getRepository('BleuebuzzShopBundle:BaseCategory');
            $entity = $repo->findOneBySlug($slug);

            if (!$entity)
            {
                throw $this->createNotFoundException('Unable to find BaseCategory entity.');
            }

            $categoryManager = $this->container->get('bleuebuzz_shop.category_manager');

            if ($form->get('recursive')->getData())
            {
                $categoryManager->recursiveRemove($entity);
            }
        }

        return $this->redirect($this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_index'));
    }

    /**
     * Up a BaseCategory entity.
     */
    public function upAction(Request $request, $slug)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BleuebuzzShopBundle:BaseCategory')->findOneBySlug($slug);
        if (!$entity)
        {
            throw $this->createNotFoundException('Unable to find BaseCategory entity.');
        }

        if (!is_null($entity->getParent()))
        {
            $option = 1;
            if ($request->attributes->get('_route') === 'admin_category_first_'.$request->get('_locale'))
            {
                $option = TRUE;
            }
            $em->getRepository('BleuebuzzShopBundle:BaseCategory')->moveUp($entity, $option);
        }

        return $this->redirect($this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_index'));
    }

    /**
     * Down a BaseCategory entity.
     *
     */
    public function downAction(Request $request, $slug)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BleuebuzzShopBundle:BaseCategory')->findOneBySlug($slug);
        if (!$entity)
        {
            throw $this->createNotFoundException('Unable to find BaseCategory entity.');
        }

        if (!is_null($entity->getParent()))
        {
            $option = 1;
            if ($request->attributes->get('_route') === 'admin_category_last_'.$request->get('_locale'))
            {
                $option = TRUE;
            }
            $em->getRepository('BleuebuzzShopBundle:BaseCategory')->moveDown($entity, $option);
        }

        return $this->redirect($this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_index'));
    }

    /**
     * Finds and displays ShopProducts of a BaseCategory entity.
     *
     * @param string $slug
     */
    public function showProductsAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BleuebuzzShopBundle:BaseCategory')->findOneBySlug($slug);

        return $this->render('BleuebuzzShopBundle:AdminBaseCategory:showProducts.html.twig', array(
            'entity'      => $entity
        ));
    }

    /**
     * Renders a form to delete a BaseCategory entity by slug.
     *
     * @param ProductInterface  $slug
     * @param CategoryInterface $category
     *
     * @return type
     */
    public function renderDeleteFormAction($slug, $recursive = FALSE)
    {
        $deleteForm = $this->createDeleteForm($slug, $recursive);

        return $this->render('BleuebuzzShopBundle:AdminBaseCategory:Render/deleteForm.html.twig', array(
            'delete_form' => $deleteForm->createView()
            )
        );
    }

    /**
     * Creates a form to delete a BaseCategory entity by slug.
     *
     * @param mixed $slug The entity slug
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm($slug, $recursive = FALSE)
    {
        $formBuilder = $this->createFormBuilder();

        if ($recursive)
        {
            $formBuilder->add('recursive', 'hidden', array('attr' => array('value' => $recursive)));
            $action = $this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_delete_recursive', array('slug' => $slug));
            $label = 'category.delete-recursive';
        }
        else
        {
            $action = $this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_delete', array('slug' => $slug));
            $label = 'category.delete';
        }

        return $formBuilder
            ->setAction($action)
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans($label, array(), 'button&link')))
            ->getForm()
        ;
    }
}