<?php

namespace Bleuebuzz\ShopBundle\Controller;

use Bleuebuzz\IntegrationBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

use Bleuebuzz\ShopBundle\Entity\CategoryInterface;
use Bleuebuzz\ShopBundle\Entity\ProductInterface;

use Bleuebuzz\ShopBundle\Entity\BaseCategory as Category;
use Bleuebuzz\ShopBundle\Entity\BaseProduct as Product;

class AdminBaseController extends BaseController
{
    /**
     * Lists all ShopCategory entities.
     */
    public function indexAction()
    {
        return $this->render('BleuebuzzShopBundle:AdminBaseCommon:index.html.twig');
    }
}
