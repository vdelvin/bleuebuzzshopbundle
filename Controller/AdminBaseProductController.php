<?php

namespace Bleuebuzz\ShopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Bleuebuzz\IntegrationBundle\Controller\BaseController;

use Bleuebuzz\ShopBundle\Entity\CategoryInterface;
use Bleuebuzz\ShopBundle\Entity\ProductInterface;

use Bleuebuzz\ShopBundle\Form\BaseProductType;

use Bleuebuzz\ShopBundle\Entity\BaseCategory as Category;
use Bleuebuzz\ShopBundle\Entity\BaseProduct as Product;
use Bleuebuzz\ShopBundle\Form\BaseCategoryType;

/**
 * AdminBaseProduct controller.
 */
class AdminBaseProductController extends BaseController
{
    /**
     * Lists all BaseProduct entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BleuebuzzShopBundle:BaseProduct')->findAll();

        return $this->render('BleuebuzzShopBundle:AdminBaseProduct:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new BaseProduct entity.
     */
    public function createAction(Request $request, $category = NULL)
    {
        $entity = new Product();

        // Category workflow
        if ($category)
        {
            $em = $this->getDoctrine()->getManager();
            $category = $em->getRepository('BleuebuzzShopBundle:BaseCategory')->findOneBySlug($category);
            if (!$category)
            {
                throw $this->createNotFoundException('Unable to find BaseCategory entity.');
            }
        }

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            // Category workflow
            if ($category)
            {
                $entity->setCategory($category);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // Category workflow
            if ($category)
            {
                $response = $this->redirect($this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_product_show', array('category' => $category->getSlug(), 'slug' => $entity->getSlug())));
            }
            else
            {
                $response = $this->redirect($this->get('bleuebuzz.trans_route')->generateTransUrl('admin_product_show', array('slug' => $entity->getSlug())));
            }
            return $response;
        }

        return $this->render('BleuebuzzShopBundle:AdminBaseProduct:new.html.twig', array(
            'entity'   => $entity,
            'form'     => $form->createView(),
            'category' => $category
        ));
    }

    /**
    * Creates a form to create a BaseProduct entity.
    *
    * @param BaseProduct $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    protected function createCreateForm(Product $entity, $category = NULL)
    {
        // Category workflow
        if ($category)
        {
            $action = $this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_product_create', array('category' => $category->getSlug()));
        }
        else
        {
            $action = $this->get('bleuebuzz.trans_route')->generateTransUrl('admin_product_create');
        }

        $form = $this->createForm(new BaseProductType($category), $entity, array(
            'action'   => $action,
            'method'   => 'POST'
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new BaseProduct entity.
     */
    public function newAction($category = NULL)
    {
        $entity = new Product();

        // Category workflow
        if ($category)
        {
            $em = $this->getDoctrine()->getManager();
            $category = $em->getRepository('BleuebuzzShopBundle:BaseCategory')->findOneBySlug($category);
            if (!$category)
            {
                throw $this->createNotFoundException('Unable to find BaseCategory entity.');
            }
            $entity->setCategory($category);
        }

        $form   = $this->createCreateForm($entity, $category);

        return $this->render('BleuebuzzShopBundle:AdminBaseProduct:new.html.twig', array(
            'entity'   => $entity,
            'form'     => $form->createView(),
            'category' => $category
        ));
    }

    /**
     * Finds and displays a BaseProduct entity.
     */
    public function showAction($slug, $category = NULL)
    {
        $entity = $this->retrieveProductWithTranslation(array('slug' => $slug));

        // Category workflow
        if ($category)
        {
            $category = $entity->getCategory();
        }

        return $this->render('BleuebuzzShopBundle:AdminBaseProduct:show.html.twig', array(
            'entity'      => $entity,
            'category'    => $category
            )
        );
    }

    /**
     * Displays a form to edit an existing BaseProduct entity.
     */
    public function editAction($slug, $category = NULL)
    {
        $entity = $this->retrieveProductWithTranslation(array('slug' => $slug));

        // Category workflow
        if ($category)
        {
            $category = $entity->getCategory();
        }

        $editForm = $this->createEditForm($entity, $category);

        return $this->render('BleuebuzzShopBundle:AdminBaseProduct:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'category'    => $category
        ));
    }

    /**
    * Creates a form to edit a BaseProduct entity.
    *
    * @param BaseProduct $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    protected function createEditForm(Product $entity, $category = NULL)
    {
        if ($category)
        {
            $action = $this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_product_update', array('category' => $category, 'slug' => $entity->getSlug()));
        }
        else
        {
            $action = $this->get('bleuebuzz.trans_route')->generateTransUrl('admin_product_update', array('slug' => $entity->getSlug()));
        }

        $form = $this->createForm(new BaseProductType(), $entity, array(
            'action' => $action,
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing BaseProduct entity.
     */
    public function updateAction(Request $request, $slug, $category = NULL)
    {
        $entity = $this->retrieveProductWithTranslation(array('slug' => $slug));

        $editForm = $this->createEditForm($entity, $category);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            if ($category)
            {
                $response = $this->redirect($this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_product_edit', array('category' => $category, 'slug' => $entity->getSlug())));
            }
            else
            {
                $response = $this->redirect($this->get('bleuebuzz.trans_route')->generateTransUrl('admin_product_edit', array('slug' => $entity->getSlug())));
            }

            return $response;
        }

        return $this->render('BleuebuzzShopBundle:AdminBaseProduct:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'category'    => $category
        ));
    }

    /**
     * Deletes a BaseProduct entity.
     */
    public function deleteAction(Request $request, $slug, $category = NULL)
    {
        $em = $this->getDoctrine()->getManager();

        // Category workflow
        if ($category)
        {
            $category = $em->getRepository('BleuebuzzShopBundle:BaseCategory')->findOneBySlug($category);
        }

        $form = $this->createDeleteForm($slug, $category);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $entity = $this->retrieveProductWithTranslation(array('slug' => $slug));
            $em->remove($entity);
            $em->flush();
        }

        // Category workflow
        if ($category)
        {
            $redirection = $this->redirect($this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_products_show', array('slug' => $category->getSlug())));
        }
        else
        {
            $redirection = $this->redirect($this->get('bleuebuzz.trans_route')->generateTransUrl('admin_product_index'));
        }

        return $redirection;
    }

    /**
     * Renders a form to delete a BaseProduct entity by slug.
     *
     * @param ProductInterface  $slug
     * @param CategoryInterface $category
     *
     * @return type
     */
    public function renderDeleteFormAction(ProductInterface $product, CategoryInterface $category = NULL)
    {
        $deleteForm = $this->createDeleteForm($product->getSlug(), $category);

        return $this->render('BleuebuzzShopBundle:AdminBaseProduct:Render/deleteForm.html.twig', array(
            'delete_form' => $deleteForm->createView()
            )
        );
    }

    /**
     * Creates a form to delete a BaseProduct entity by slug.
     *
     * @param mixed $slug The entity slug
     *
     * @return Form The form
     */
    protected function createDeleteForm($slug, CategoryInterface $category = NULL)
    {
        if ($category)
        {
            $action = $this->get('bleuebuzz.trans_route')->generateTransUrl('admin_category_product_delete', array('category' => $category->getSlug(), 'slug' => $slug));
        }
        else
        {
            $action = $this->get('bleuebuzz.trans_route')->generateTransUrl('admin_product_delete', array('slug' => $slug));
        }

        return $this->createFormBuilder()
            ->setAction($action)
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('delete', array(), 'form')))
            ->getForm();
    }

    /**
     * Retrieve product with translated property
     *
     * @param array $options
     */
    private function retrieveProductWithTranslation(array $options)
    {
        $em = $this->getDoctrine()->getManager();

        $property = key($options);
        $repositoryMethod = 'findOneBy'.ucfirst($property);

        // Default
        $entity = $em->getRepository('BleuebuzzShopBundle:BaseProduct')->$repositoryMethod($options[$property]);

        // Try with translation
        if (!$entity && $this->get('translator')->getFallbackLocales() !== $this->getRequest()->getSession()->get('_locale'))
        {
            $translationRepository = $em->getRepository('Gedmo\Translatable\Entity\Translation');
            $entityTranslation = $translationRepository->findOneBy(
                array(
                    'objectClass' => 'Bleuebuzz\ShopBundle\Entity\BaseProduct',
                    'field'       => $property,
                    'content'     => $options[$property],
                    'locale'      => $this->getRequest()->getSession()->get('_locale')
                )
            );

            if (!is_null($entityTranslation))
            {
                $entity = $em->getRepository('BleuebuzzShopBundle:BaseProduct')->find($entityTranslation->getForeignKey());
            }
        }

        if (!$entity)
        {
            throw $this->createNotFoundException('Unable to find BaseProduct entity.');
        }

        return $entity;
    }
}
