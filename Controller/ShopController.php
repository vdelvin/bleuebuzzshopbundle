<?php

namespace Bleuebuzz\ShopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Bleuebuzz\ShopBundle\Entity\Category;
use Bleuebuzz\ShopBundle\Form\CategoryType;

class ShopController extends Controller
{
    /**
     * Lists all Category entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BleuebuzzShopBundle:Category')->findAll();

        return $this->render('BleuebuzzShopBundle:Shop:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Finds and displays a Category entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BleuebuzzShopBundle:Category')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BleuebuzzShopBundle:Shop:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }
}
