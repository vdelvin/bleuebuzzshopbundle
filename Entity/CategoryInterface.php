<?php

namespace Bleuebuzz\ShopBundle\Entity;

use Doctrine\Common\Collections\Collection;

/**
 * CategoryInterface
 */
interface CategoryInterface
{
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId();

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string 
     */
    public function getName();

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description);

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Category
     */
    public function setUpdated($updated);

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated();

    /**
     * Set lft
     *
     * @param integer $lft
     *
     * @return Category
     */
    public function setLft($lft);

    /**
     * Get lft
     *
     * @return integer 
     */
    public function getLft();

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return Category
     */
    public function setLvl($lvl);

    /**
     * Get lvl
     *
     * @return integer 
     */
    public function getLvl();

    /**
     * Set rgt
     *
     * @param integer $rgt
     *
     * @return Category
     */
    public function setRgt($rgt);

    /**
     * Get rgt
     *
     * @return integer 
     */
    public function getRgt();

    /**
     * Set root
     *
     * @param integer $root
     *
     * @return Category
     */
    public function setRoot($root);

    /**
     * Get root
     *
     * @return integer 
     */
    public function getRoot();

    /**
     * Set parent
     *
     * @param CategoryInterface $parent
     *
     * @return Category
     */
    public function setParent(CategoryInterface $parent);

    /**
     * Get parent
     *
     * @return CategoryInterface 
     */
    public function getParent();

    /**
     * Set children
     *
     * @param array $children
     *
     * @return Category
     */
    public function setChildren(Collection $children);

    /**
     * Get children
     *
     * @return array 
     */
    public function getChildren();

    /**
     * Get products
     *
     * @return Collection
     */
    public function getProducts();

    /**
     * Set products
     *
     * @param Collection $products
     */
    public function setProducts(ProductInterface $products);

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Category
     */
    public function setCreated($created);

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated();

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug();

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug);

    /**
     * __toString
     *
     * @return type
     */
    public function __toString();
}
