<?php

namespace Bleuebuzz\ShopBundle\Entity;

/**
 * ProductInterface
 */
interface ProductInterface
{
    public function getId();

    public function getCategory();

    public function setCategory(CategoryInterface $category);

    public function getName();

    public function setName($name);

    public function getPrice();

    public function setPrice($price);

    public function getDescription();

    public function setDescription($description);

    public function getCreated();

    public function setCreated($created);

    public function getUpdated();

    public function setUpdated($updated);

    public function getSlug();

    public function setSlug($slug);

    public function setTranslatableLocale($locale);
}