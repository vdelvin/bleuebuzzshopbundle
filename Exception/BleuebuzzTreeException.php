<?php

namespace Bleuebuzz\ShopBundle\Exception;

/**
 * BleuebuzzTreeException
 *
 * @author Vincent Delvinquière <vdelvin@gmail.com>
 */
class BleuebuzzTreeException extends \RuntimeException
{
    private $headers;
    protected $message = 'The nested tree is broken !!!';

    public function __construct($message = null, \Exception $previous = null, array $headers = array(), $code = 0)
    {
        $this->headers = $headers;
        if (!is_null($message))
        {
            $this->message = $message;
        }

        parent::__construct($this->message, $code, $previous);
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getHeaders()
    {
        return $this->headers;
    }
}
