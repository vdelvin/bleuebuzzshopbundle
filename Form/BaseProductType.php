<?php

namespace Bleuebuzz\ShopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Bleuebuzz\ShopBundle\Form\EventListener\AddNewProductCategoryFieldSubscriber;
use Bleuebuzz\ShopBundle\Entity\BaseCategory;

class BaseProductType extends AbstractType
{
    private $category;

    public function __construct(BaseCategory $category = NULL)
    {
        $this->category = $category;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('price')
        ;

        $builder->addEventSubscriber(new AddNewProductCategoryFieldSubscriber($this->category));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bleuebuzz\ShopBundle\Entity\BaseProduct',
            'translation_domain' => 'form'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bleuebuzz_shopbundle_baseproduct';
    }
}
