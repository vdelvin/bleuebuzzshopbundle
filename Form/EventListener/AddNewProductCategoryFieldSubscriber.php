<?php

namespace Bleuebuzz\ShopBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Bleuebuzz\ShopBundle\Entity\BaseCategory;
use Doctrine\Common\Collections\Collection;

/**
 * Description of addNewAdjustingFieldsListener
 *
 * @author vdelvin
 */
class AddNewProductCategoryFieldSubscriber implements EventSubscriberInterface
{
    private $category;

    public function __construct(BaseCategory $category = NULL)
    {
        $this->category = $category;
    }

    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(FormEvent $event)
    {
        $product = $event->getData();
        $form = $event->getForm();

        // We want a hidden category field if we are in category workflow
        if (is_null($this->category))
        {
            $form->add('category');
        }
    }
}