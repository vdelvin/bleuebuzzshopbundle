<?php

namespace Bleuebuzz\ShopBundle\Listener;

use Doctrine\Common\EventArgs;
use Gedmo\Mapping\MappedEventSubscriber;
use Gedmo\Sluggable\Mapping\Event\SluggableAdapter;
use Doctrine\Common\Persistence\ObjectManager;
use Gedmo\Sluggable\SluggableListener;
use Bleuebuzz\ShopBundle\Entity\ProductInterface;

/**
 * The SluggableListener handles the generation of slugs
 * for documents and entities.
 *
 * This behavior can inpact the performance of your application
 * since it does some additional calculations on persisted objects.
 *
 * @author Gediminas Morkevicius <gediminas.morkevicius@gmail.com>
 * @author Klein Florian <florian.klein@free.fr>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
class GedmoSluggableListener extends SluggableListener
{
    /**
     * Specifies the list of events to listen
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        $events = parent::getSubscribedEvents();

        // Adds postPersist for slug creation with id
        $events[] = 'postPersist';

        return $events;
    }

    /**
     * Allows identifier fields to be slugged as usual
     *
     * @param EventArgs $args
     * @return void
     */
    public function postPersist(EventArgs $args)
    {
        $ea = $this->getEventAdapter($args);
        $om = $ea->getObjectManager();
        $object = $ea->getObject();

        if ($object instanceof  ProductInterface)
        {
            $object->setSlug($object->getId().'-'.$object->getSlug());
            $ea = $this->getEventAdapter($args);
            $om = $ea->getObjectManager();
            $om->persist($object);
            $om->flush();
        }
    }
}
