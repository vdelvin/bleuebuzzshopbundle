<?php

namespace Bleuebuzz\ShopBundle\Manager;

use Doctrine\ORM\EntityManager;
use Bleuebuzz\ShopBundle\Exception\BleuebuzzTreeException;
use Bleuebuzz\ShopBundle\Entity\CategoryInterface;

/**
 * BaseCategoryManager
 *
 * @author Vincent Delvinquière <vdelvin@gmail.com>
 */
class BaseCategoryManager
{
    private $em;
    private $categoryRepository;

    /**
     * __construct
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->categoryRepository = $this->em->getRepository('BleuebuzzShopBundle:BaseCategory');
    }

    /**
     * Remove a node and his children
     *
     * @param CategoryInterface $category
     *
     * @return boolean
     */
    public function recursiveRemove(CategoryInterface $category)
    {
        $this->em->remove($category);

        return $this->checkAndTryToRepairTree();
    }

    /**
     * Remove a single node
     *
     * @param CategoryInterface $category
     *
     * @return boolean
     */
    public function nodeRemove(CategoryInterface $category)
    {
        // Retrieve children ids array without ORM manipulation
        $sql = "SELECT id FROM shop_categories WHERE parent_id = ?";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue(1, $category->getId());
        $stmt->execute();
        $childrenIds = $stmt->fetch();
        $removedCategory = array(
            'class' => get_class($category),
            'id'    => $category->getId(),
            'slug'  => $category->getSlug(),
            'lvl'   => $category->getLvl()
        );

        $this->categoryRepository->removeFromTree($category);

        if ($this->checkAndTryToRepairTree())
        {
            // Update children slug
            if ($childrenIds)
            {
                $this->updateChildrenSlugAfterNodeRemove($removedCategory, $childrenIds);
            }

            // No translatable handler here, we've to manually delete translations
            $translationRepository = $this->em->getRepository('Gedmo\Translatable\Entity\Translation');
            $translationsToRemoved = $translationRepository->findBy(array('objectClass' => $removedCategory['class'], 'foreignKey' => $removedCategory['id']));
            if (count($translationsToRemoved) > 0)
            {
                foreach ($translationsToRemoved as $translation)
                {
                    $this->em->remove($translation);
                }
            }
        }

        return $this->checkAndTryToRepairTree();
    }

    /**
     * Update children slug when remove parent
     *
     * @todo very bad hack to update slug children. Gedmo tree and slug extension don't manage that on remove. Try to include it in remove transaction
     *
     * @param array                 $removedCategory
     * @param array|ArrayCollection $children
     *
     * @throws BleuebuzzTreeException
     */
    private function updateChildrenSlugAfterNodeRemove($removedCategory, $children)
    {
        // Retrieve the slug part to remove
        $slugExplode = explode('/', $removedCategory['slug']);
        $slugPartToRemoved = end($slugExplode);
        $catSlugLvlToRemoved = $removedCategory['lvl'];

        foreach ($children as $category)
        {
            if (!is_object($category))
            {
                $category = $this->categoryRepository->find($category);
            }

            // Build new slug
            $childSlugExplode = explode('/', $category->getSlug());
            if ($childSlugExplode[$catSlugLvlToRemoved] === $slugPartToRemoved)
            {
                unset($childSlugExplode[$catSlugLvlToRemoved]);
            }
            else
            {
                throw new BleuebuzzTreeException('The category tree slugs are broken for root'.$category->getRoot());
            }
            $newSlug = implode('/', $childSlugExplode);

            // Execute bdd update
            $sql = "UPDATE shop_categories SET slug = ? WHERE id = ?";
            $stmt = $this->em->getConnection()->prepare($sql);
            $stmt->bindValue(1, $newSlug);
            $stmt->bindValue(2, $category->getId());
            $stmt->execute();

            // Children recursion
            if ($category->getChildren()->count() > 0)
            {
                $this->updateChildrenSlugAfterNodeRemove($removedCategory, $category->getChildren());
            }
        }
    }
    
    /**
     * Check and try to repair nested tree
     *
     * @throws BleuebuzzTreeException
     * @return boolean
     */
    private function checkAndTryToRepairTree()
    {
        if ($this->categoryRepository->verify() !== TRUE)
        {
            $this->categoryRepository->recover();
            $hasErrors = $this->categoryRepository->verify();

            if (is_array($hasErrors))
            {
                $this->em->clear();
                $message = implode(' - ', $hasErrors);
                throw new BleuebuzzTreeException($message);
            }
        }

        $this->em->flush();
        $this->em->clear();

        return TRUE;
    }
}
