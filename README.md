BleuebuzzShopBundle
==========================

Configuration du bundle dans un projet Symfony
-----------------------------------------------

    * ajouter au composer.json
        "require": {
            ....
            "gedmo/doctrine-extensions": "dev-master",
            "bleuebuzz/shop-bundle": "dev-master",
            ....
        }

    * lancer la cmd "php composer.phar update"

    * ajouter au fichier app/AppKernel.php
        new Bleuebuzz\ShopBundle\BleuebuzzShopBundle()
