<?php

namespace Bleuebuzz\ShopBundle\Repository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

/**
 * CategoryRepository
 *
 * @author Vincent Delvinquière <vdelvin@gmail.com>
 */
class BaseCategoryRepository extends NestedTreeRepository
{
}
